# Desafio de programação Back-end

Desafio de programação Back-end

Implemente uma API REST para realizar o gerenciamento de funcionários e empresas de um sistema. Neste cenário, cada funcionário pode pertencer a uma ou mais empresas, assim como cada empresa pode possuir um ou mais funcionários. As operações básicas de um CRUD deverão ser implementadas com os verbos HTTP e códigos de retorno HTTP apropriados, tanto para funcionários, como para empresas. 


## Requisitos: 

* Utilizar NodeJS com Typescript 
* Na exibição dos dados do funcionário deverão ser mostradas as empresas as quais ele está associado, assim como na exibição dos dados da empresa os funcionários vinculados à ela deverão ser exibidos. 
* Os campos de cada objeto funcionário são: nome, cpf, e-mail e endereço. 
* Os campos de cada objeto empresa são: nome, cnpj e endereço. 
* Utilize padrões de projeto que você considera ideal. 
* Procure realizar um tratamento de erros adequado 


## Diferencial: 

* Para o banco de dados utilize PostgreSQL 
* Utilizar GIT CI/CD para simular a automatização dos testes 

# Como executar o servidor da API REST deste projeto

![](header.jpg)

## Download 

Execute o download do projeto ou clone do GIT repositorio

[https://gitlab.com/hesete/react-typescript.git](https://gitlab.com/hesete/react-typescript.git)

# Crie tabelas do banco de dados postgresql

Crie as tabelas do banco de dados utilizando o arquivo deste projeto /src/config/setup.sql

## Conetar o postgresql ao projeto

Editar o arquivo /prisma/.env a linha abaixo

```code
DATABASE_URL="postgresql://USUARIO:SENHA@localhost:5432/BANCODEDADOS?schema=public"
```

* USUARIO = usuario do postgres com privilegios de editar o BANCODEDADOS
* SENHA = senha do usuario do postgres
* BANCODEDADOS = banco de dados no postresql com as tabelas deste projeto

## Instalar o projeto

Execute o comando abaixo na pasta do projeto
```sh
npm install
```

## Inicializar o projeto

Este projeto esta utilizando a porta http 8000 como padrão
```sh
npm start
```

# Interface de utilização

### Usar json no Request e Response
### Leia [src/funcionario/README.md](src/funcionario/README.md) para mais detalhes
### Leia [src/empresa/README.md](src/empresa/README.md) para mais detalhes

### Codigos http de resposta:

* Para simplificar o tratamento de erros utilizei apenas 3 codigos http.

    * 200 - sucesso na operação
    * 400 - erro em condições de atributos enviados
    * 500 - erro relacionado ao banco de dados

### URLs Empresa

* GET   http://127.0.0.1:8000/empresa
* GET   http://127.0.0.1:8000/empresa/(id)
* POST  http://127.0.0.1:8000/empresa
* PUT   http://127.0.0.1:8000/empresa/(id)
* DEL   http://127.0.0.1:8000/empresa/(id)

### URLs Funcionario

* GET   http://127.0.0.1:8000/funcionario
* GET   http://127.0.0.1:8000/funcionario/(id)
* POST  http://127.0.0.1:8000/funcionario
* PUT   http://127.0.0.1:8000/funcionario/(id)
* DEL   http://127.0.0.1:8000/funcionario/(id)

# Ambiente utilizado no desenvolvimento do projeto

## Sistema Operacional

* Windows 10 Pro 64 19041

## Instalar Node.js

* https://nodejs.org/pt-br/download/package-manager/

## Instalar Visual Studio Code

* https://code.visualstudio.com/docs/setup/windows

## Instalar Insomnia

* https://insomnia.rest/download/

## Instalar Postgresql

* https://www.postgresql.org/download/windows/

## Instalar yarn  (opcional ao npm)

* https://yarnpkg.com/lang/pt-BR/docs/install/

## Instalar GIT (opcional)

* https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Instalando-o-Git




# Comandos do prompt utilizados no desenvolvimento

Os comandos abaixo não são necessários para execução do projeto

Criar pasta para o projeto
```sh
mkdir typescript
cd typescript
```

Instalar typescript
```sh
npm install -g typescript
```

Iniciar projeto nodejs com typescript
```sh
npm init -y
npx tsc --init
```

Instalar no projeto express typescript ts-node ts-node-dev @types/express @types/node
```sh 
npm install express
npm install express-validator
npm install typescript --save-dev
npm install ts-node --save-dev
npm install ts-node-dev --save-dev
npm install @types/express --save-dev
npm install @types/node --save-dev
```

Editar arquivo packge.json
```code
"scripts": {
    "build":"tsc",
    "start": "ts-node-dev src\\index.ts",
```

Instalação do prisma (abstract DB)

link : [setup prisma](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch-sql-typescript-postgres)

```sh
npm install @prisma/cli --save-dev
npm install @prisma/client
npx prisma
npx prisma init
npx prisma introspect
npx prisma generate
```

Inicia backend
```sh
npm start
```