
import express from "express";
import { funcionariosRouter } from "./funcionario/funcionario.router";
import { empresasRouter } from "./empresa/empresa.router";
require('dotenv').config()




const app = express();

/**
 *  App Configuration
 */

app.use(express.json());
app.use("/funcionario", funcionariosRouter);
app.use("/empresa", empresasRouter);


export const server = app