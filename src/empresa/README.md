# Como utilizar a API Empresa

## URLs Empresa

* GET   http://127.0.0.1:8000/empresa
* GET   http://127.0.0.1:8000/empresa/(id)
* POST  http://127.0.0.1:8000/empresa
* PUT   http://127.0.0.1:8000/empresa/(id)
* DEL   http://127.0.0.1:8000/empresa/(id)

### GET http://127.0.0.1:8000/empresa

Parametros diretamente na url ou utilizando Query do Insomnia/postman
* pagina - pagina é opcional ou deve ser um numero inteiro, 1 <= pagina
* limite - limite é opcional ou deve ser um numero inteiro, 0 <= limite <= 100
* procurar - procurar é opcional ou deve conter no minimo 2 caracteres
* ordem - ordem é opcional ou deve conter um ou mais ([nome|endereco][,asc|,desc];) EX:'nome,asc;endereco,desc'

Exemplo
```code
GET http://127.0.0.1:8000/empresa?pagina=1&limite=10&procurar=Emp&ordem=nome,asc;endereco,desc
```
Codigo 200 - Retorno com sucesso
```code
[
  {
    "id": 1,
    "nome": "Empresa 01",
    "cnpj": "12345678921",
    "endereco": "Rua Prof. Herminio Jacques 300",
    "empresa_funcionario": [
      {
        "empre_id": 1,
        "funci_id": 4,
        "funcionario": {
          "id": 4,
          "nome": "Funcionario 03",
          "cpf": "",
          "email": "helio@s1te.com.br",
          "endereco": "Rua Prof. Herminio Jacques 229"
        }
      }
    ]
  }
]
```
Codigo 400 - Erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "",
      "msg": "pagina é opcional ou deve ser um numero inteiro, 1 <= pagina",
      "param": "pagina",
      "location": "query"
    },
    {
      "value": "",
      "msg": "limite é opcional ou deve ser um numero inteiro, 0 <= limite <= 100",
      "param": "limite",
      "location": "query"
    },
    {
      "value": "",
      "msg": "ordem é opcional ou deve conter um ou mais ([nome|cnpj|endereco][,asc|,desc];) EX:'nome,asc;cnpj,desc'",
      "param": "ordem",
      "location": "query"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

GET http://127.0.0.1:8000/empresa/(id)
* id - id é obrigatorio, deve ser numerico inteiro, 0 <= id existente

Exemplo
```code
GET http://127.0.0.1:8000/empresa/1
```
Codigo 200 - Retorno com sucesso
```code
null

OU 

{
  "id": 1,
  "nome": "Empresa 01",
  "cnpj": "12345678921",
  "endereco": "Rua Prof. Herminio Jacques 300",
  "empresa_funcionario": [
    {
      "empre_id": 1,
      "funci_id": 4,
      "funcionario": {
        "id": 4,
        "nome": "Funcionario 03",
        "cpf": "22265369s810",
        "email": "helio@s1te.com.br",
        "endereco": "Rua Prof. Herminio Jacques 229"
      }
    },
  ]
}
```
Codigo 400 - Erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "-1",
      "msg": "id é obrigatorio, deve ser numerico inteiro, 0 <= id",
      "param": "id",
      "location": "params"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

### DEL http://127.0.0.1:8000/empresa/(id)
* id - id é obrigatorio, deve ser numerico inteiro, 0 <= id existente

Exemplo
```code
DEL http://127.0.0.1:8000/empresa/1
```
Codigo 200 - Retorno com sucesso
```code
{}
```
Codigo 400 - Erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "-2",
      "msg": "id é obrigatorio, deve ser numerico inteiro e id > 0",
      "param": "id",
      "location": "params"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Error for binding '0': RecordNotFound(\"Record to delete does not exist.\")"
}
```

### POST http://127.0.0.1:8000/empresa
Parametros POST json
* nome - Nome é obrigatório ou deve ter no minimo 2 caracteres
* cnpj - CNPJ é opcional ou deve ser valido
* endereco - Endereço é opcional ou deve conter no minimo 2 caracteres
* funcionarios - Funcionarios é opcional ou deve conter um array de strings de numeros inteiros ids dos funcionarios

Exemplo POST com json:
```code
POST http://127.0.0.1:8000/funcionario

{
	"nome":"empresa 08",
	"cnpj":"99521539000153",
	"endereco":"Rua Prof. Herminio Jacques 229",
	"funcionarios":["2","4"]
}
```
Codigo 200 - Retorno com sucesso
```code
{
  "id": 8,
  "nome": "empresa 08",
  "cnpj": "99521539000153",
  "endereco": "Rua Prof. Herminio Jacques 229"
}
```
Codigo 400 - Erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "",
      "msg": "Nome é obrigatório",
      "param": "nome",
      "location": "body"
    },
    {
      "value": "",
      "msg": "CNPJ invalido",
      "param": "cnpj",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Endereço é opcional ou deve conter no minimo 2 caracteres",
      "param": "endereco",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Funcionarios é opcional ou deve conter um array de strings de numeros inteiros",
      "param": "funcionarios",
      "location": "body"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

### PUT http://127.0.0.1:8000/empresa/(id)
* id - id é obrigatorio, deve ser numerico inteiro, 0 <= id existente
* nome - Nome é obrigatório ou deve ter no minimo 2 caracteres
* cnpj - CNPJ é opcional ou deve ser valido
* endereco - Endereço é opcional ou deve conter no minimo 2 caracteres
* funcionarios - Funcionarios é opcional ou deve conter um array de strings de numeros inteiros ids dos funcionarios

Exemplo
```code
PUT http://127.0.0.1:8000/empresa/8

{
  "nome": "Super Empresa",
  "endereco": "Avenida beira covid, 999"
}
```
Codigo 200 - Retorno com sucesso
```code
```
Codigo 400 - Erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "-1",
      "msg": "id é obrigatorio, deve ser numerico inteiro, 0 <= id",
      "param": "id",
      "location": "params"
    },
    {
      "value": "",
      "msg": "Nome é obrigatório",
      "param": "nome",
      "location": "body"
    },
    {
      "value": "",
      "msg": "CNPJ invalido",
      "param": "cnpj",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Endereço é opcional ou deve conter no minimo 2 caracteres",
      "param": "endereco",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Funcionarios é opcional ou deve conter um array de strings de numeros inteiros",
      "param": "funcionarios",
      "location": "body"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Error for binding '0': RecordNotFound(\"Record to delete does not exist.\")"
}
```