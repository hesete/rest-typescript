import express, { Request, Response } from "express";
import { empresaCreateInput, FindManyempresaArgs, PrismaClient } from '@prisma/client'
import { body, param, query,validationResult } from "express-validator";
import {isValidCNPJ,ordenarQueryToArObj} from "../util/validar";


/**
 * Router Definition
 */
export const empresasRouter = express.Router();


const prisma = new PrismaClient()

/**
 * Controller Definitions
 */

// GET empresas/
  const atributosOrdevaveis = ["nome","cnpj","endereco"];
  const atributosProcuraveis = atributosOrdevaveis;
  empresasRouter.get("/",
  [
    query("pagina","pagina é opcional ou deve ser um numero inteiro, 1 <= pagina").optional().isInt({gt:0}),
    query("limite","limite é opcional ou deve ser um numero inteiro, 0 <= limite <= 100").optional().trim().isInt({gt:-1,lt:101}),
    query("procurar","procurar é opcional ou deve conter no minimo 2 caracteres").optional().trim(),
    query("ordem","ordem é opcional ou deve conter um ou mais ([nome|cnpj|endereco][,asc|,desc];) EX:'nome,asc;cnpj,desc'").optional().trim()
            .custom((_ordem)=>{
              return ordenarQueryToArObj(_ordem,atributosOrdevaveis)
                .filter((x:object)=>{return x===undefined})
                .length===0;
            }),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        let op : FindManyempresaArgs = {
          include: { empresa_funcionario : {include:{funcionario:true}} }
        };

        // paginacao
        let _pagina = 1;
        let _limite = 0;
        if (req.query && req.query.pagina) { _pagina = Number.parseInt(String(req.query.pagina)) || 1;}
        if (req.query && req.query.limite) { _limite = Number.parseInt(String(req.query.limite)) || 0;}
        if(_limite>0){
          op.skip = ((_pagina-1)*_limite);
          op.take = _limite;
        }
        
        // procurar
        if(req.query && req.query.procurar && String(req.query.procurar).length > 1){
          let procurar = atributosProcuraveis.map((x:any)=>{return {[x]:{contains:req.query.procurar}};})
          op.where = {OR:procurar}
        }

        // ordenar
        let ordem = ordenarQueryToArObj(req.query.ordem,atributosOrdevaveis)
                      .filter((x:object)=>{return x!=undefined})
        if(ordem.length>0) op.orderBy = ordem
        
        // banco de dados
        await prisma.empresa.findMany(op).then((values)=>{
            res.status(200).json(values);
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });

  // GET empresas/:id
  
  empresasRouter.get("/:id",
  [
    param("id","id é obrigatorio, deve ser numerico inteiro, 0 <= id").isInt({gt:0})
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const id: number= parseInt(req.params.id, 10);
        
        // banco de dados
        await prisma.empresa.findOne({
            where:{id:id},
            include: { 
              empresa_funcionario : {
                include:{funcionario:true}
              } 
            }
        }).then((values)=>{
            res.status(200).json(values);
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
  
  
  // POST empresas/

  empresasRouter.post("/",
  [
    body("nome","Nome é obrigatório").isLength({min:1}).trim(),
    body("cnpj","CNPJ invalido").optional().trim().custom(isValidCNPJ),
    body("endereco","Endereço é opcional ou deve conter no minimo 2 caracteres").optional().isLength({min:2}),
    body("funcionarios","Funcionarios é opcional ou deve conter um array de strings de numeros inteiros").optional().isArray()
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const {nome,cnpj,endereco, funcionarios} = req.body;

        const data : empresaCreateInput = {nome:nome.trim()};
        if(cnpj!=undefined){data.cnpj = cnpj.trim().replace(/[\s.-//]*/igm, '');}
        if(endereco!=undefined){data.endereco = endereco.trim();}

        // Adiciona relação de empresa com funcionario
        const create_funcionario = new Array();
        if(Array.isArray(funcionarios)&&funcionarios.length>0){
          Array.from(new Set(funcionarios)).forEach((func)=>{
            const funci_id:number = Number.parseInt(func)
            if(!isNaN(funci_id))
              create_funcionario.push({
                funcionario:{
                  connect:{id:funci_id}}}
              );
          })
          if(create_funcionario.length>0)
            data.empresa_funcionario = {create:create_funcionario}
        }
        
        // banco de dados
        await prisma.empresa.create({
            data:data
        }).then((values)=>{
            res.status(200).json(values);
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
  
  // PUT empresas/
  
  empresasRouter.put("/:id",
  [
    param("id","id é obrigatorio, deve ser numerico inteiro, 0 <= id").isInt({gt:0}),
    body("nome","Nome é obrigatório").optional().trim().isLength({min:2}),
    body("cnpj","CNPJ invalido").optional().trim().custom(isValidCNPJ),
    body("endereco","Endereço é opcional ou deve conter no minimo 2 caracteres").optional().isLength({min:2}),
    body("funcionarios","Funcionarios é opcional ou deve conter um array de strings de numeros inteiros").optional().isArray()
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const id: number = parseInt(req.params.id, 10);
        const {nome,cnpj, endereco, funcionarios} = req.body;

        const data : {[k: string]: any} = {};
        if(nome!=undefined){data.nome = nome.trim();}
        if(cnpj!=undefined){data.cnpj = cnpj.trim().replace(/[\s.-//]*/igm, '');}
        if(endereco!=undefined){data.endereco = endereco.trim();}

        //Adiciona relação de empresa com funcionario
        const create_funcionario = new Array();
        if(Array.isArray(funcionarios)&&funcionarios.length>0){
          Array.from(new Set(funcionarios)).forEach((func)=>{
            const funci_id:number = Number.parseInt(func)
            if(!isNaN(funci_id))
              create_funcionario.push({
                funcionario:{
                  connect:{id:funci_id}}}
              );
          })
          if(create_funcionario.length>0)
            data.empresa_funcionario = {create:create_funcionario}
        }
        
        // banco de dados
        await prisma.empresa_funcionario.deleteMany({
          where:{empre_id:id}
        }).then(async ()=>{
          // banco de dados
          await prisma.empresa.update({
              where:{id:id},
              data:data
          }).then((values)=>{
              res.status(200).json(values);
          }).catch((e)=>{
            res.status(500).json({ erro:e.message});
          })
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
  
  // DELETE empresas/:id
  
  empresasRouter.delete("/:id",
  [
    param("id","id é obrigatorio, deve ser numerico inteiro e id > 0").isInt({gt:0})
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const id: number = parseInt(req.params.id, 10);
        
        // banco de dados
        await prisma.empresa_funcionario.deleteMany({
          where:{empre_id:id}
        }).then(async (emp_fun)=>{
          // banco de dados
          await prisma.empresa.delete({
              where:{id:id}
          }).then((values)=>{
              res.status(200).json({});
          }).catch((e)=>{
            res.status(500).json({ erro:e.message});
          })
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        });
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
