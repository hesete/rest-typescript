

/*
CREATE DATABASE bry_typescript
    WITH 
    OWNER = Helio
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;
*/

ALTER TABLE IF EXISTS empresa_funcionario DROP CONSTRAINT IF EXISTS fk_empresa;
ALTER TABLE IF EXISTS empresa_funcionario DROP CONSTRAINT IF EXISTS fk_funcionario;

DROP TABLE IF EXISTS empresa_funcionario;
DROP TABLE IF EXISTS empresa;
DROP TABLE IF EXISTS funcionario;


CREATE TABLE empresa (
    id serial primary key,
    nome VARCHAR(255) NOT NULL,
    cnpj VARCHAR(20),
    endereco text
);

CREATE TABLE funcionario (
    id serial primary key,
    nome VARCHAR(255) NOT NULL,
    cpf VARCHAR(15),
    email VARCHAR(255),
    endereco text
);

CREATE TABLE empresa_funcionario(
    empre_id INT NOT NULL,
    funci_id INT NOT NULL,
    PRIMARY KEY(empre_id,funci_id)
);

ALTER TABLE empresa_funcionario ADD CONSTRAINT fk_empresa FOREIGN KEY(empre_id) REFERENCES empresa(id) ON DELETE CASCADE;
ALTER TABLE empresa_funcionario ADD CONSTRAINT fk_funcionario FOREIGN KEY(funci_id) REFERENCES funcionario(id) ON DELETE CASCADE;
    
