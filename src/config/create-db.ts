const { Pool } = require('pg')
require('dotenv').config()

const DB_URL = process.env.DATABASE_URL||`postgres://postgres@postgres:5432/postgres`;

console.log(DB_URL);
const DB = {
  connectionString: DB_URL
}

const pg = new Pool(DB)

let contTryConnet = 0;
async function tryConnect(){
   try{
        await pg.connect()
    }catch(e){
        let end = await setInterval(async()=>{
            await tryConnect();
            clearInterval(end);
            contTryConnet++;
            //if(contTryConnet>5) process.exit(1)
        },5000)
    }
}
async function createDB(){
        await pg.query('ALTER TABLE IF EXISTS empresa_funcionario DROP CONSTRAINT IF EXISTS fk_empresa')
        await pg.query('ALTER TABLE IF EXISTS empresa_funcionario DROP CONSTRAINT IF EXISTS fk_funcionario')
        await pg.query('DROP TABLE IF EXISTS empresa_funcionario')
        await pg.query('DROP TABLE IF EXISTS empresa')
        await pg.query('DROP TABLE IF EXISTS funcionario')
        await pg.query("CREATE TABLE empresa (id serial primary key,nome VARCHAR(255) NOT NULL,cnpj VARCHAR(20),endereco text)")
        await pg.query('CREATE TABLE funcionario (id serial primary key,nome VARCHAR(255) NOT NULL,cpf VARCHAR(15),email VARCHAR(255),endereco text)')
        await pg.query('CREATE TABLE empresa_funcionario(empre_id INT NOT NULL,funci_id INT NOT NULL,PRIMARY KEY(empre_id,funci_id))')
        await pg.query('ALTER TABLE empresa_funcionario ADD CONSTRAINT fk_empresa FOREIGN KEY(empre_id) REFERENCES empresa(id) ON DELETE CASCADE')
        await pg.query('ALTER TABLE empresa_funcionario ADD CONSTRAINT fk_funcionario FOREIGN KEY(funci_id) REFERENCES funcionario(id) ON DELETE CASCADE')
}

createDB()
