import {server} from "../app";
import supertest from 'supertest'
import  http  from 'http'
let s = http.createServer(server)
const request = supertest(server)
const apis = ["empresa","funcionario"]
const metodos_id = ["GET","PUT","DEL"]

afterAll(()=>{})
beforeAll(()=>{})
beforeEach(()=>{})
afterEach(()=>{})
apis.forEach((api)=>{
  describe("\n TESTE HTTP API REST "+api.toUpperCase(),()=>{

      it('test 200 GET http://.../'+api+'/', async done => {
        request.get('/'+api).then((response)=>{

          expect(response.status).toBe(200)
          done()
        })
      })

      it('test 200 GET http://.../'+api+'?pagina=1&limite=2&ordem=nome,asc;endereco,desc&procurar=Rua', async done => {
        const response = await request.get('/'+api+'?pagina=1&limite=2&ordem=nome,asc;endereco,desc&procurar=Rua')
        expect(response.status).toBe(200)
        done()
      })

      metodos_id.forEach((mtd)=>{
        it('test 200 '+mtd+' http://.../'+api+'/100', async done => {
          const response = await request.get('/'+api+'/100')
          expect(response.status).toBe(200)
          done()
        })
        it('test 400 '+mtd+' http://.../'+api+'/-100', async done => {
          const response = await request.get('/'+api+"/-100")
          expect(response.status).toBe(400)
          done()
        })
      })

      it('test 200 POST http://.../'+api+'', async () => {
        const response = await request.post('/'+api).send(
          {"nome":"TESTE "+api}
        );
        expect(response.status).toBe(200)
        expect(response.body.nome).toContain("TESTE "+api);
      });

      it('test 400 POST http://.../'+api+'', async () => {
        const response = await request.post('/'+api).send(
          {"nome":""}
        );
        expect(response.status).toBe(400)
      });

      it('test 200 PUT http://.../'+api+'/(id)', async () => {
        const response = await request.post('/'+api).send(
          {"nome":"TESTE "+api}
        );
        const put_response = await request.put('/'+api+'/'+response.body.id).send(
          {"nome":"TESTE PUT "+api}
        );
        expect(put_response.status).toBe(200)
        expect(put_response.body.nome).toContain("TESTE PUT "+api);
      });
    })
  })


