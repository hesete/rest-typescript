import { Pool, Client }  from 'pg';
import ThenPromise from 'promise';
require('dotenv').config()


const DB_URL = process.env.DATABASE_URL||`postgres://postgres@postgres:5432/postgres`;

const DB = {
  connectionString: DB_URL
}

let pg = new Pool(DB)


beforeAll(()=>{})
beforeEach(()=>{})
afterEach(()=>{})
describe("\n TESTE DE BANCO DE DADOS ",()=>{
  afterAll(()=>{pg.end()})
  
    it('DELETAR CONSTRAINT', async done => {
          
          pg.connect()
          .then(async ()=>{
            await pg.query('ALTER TABLE IF EXISTS empresa_funcionario DROP CONSTRAINT IF EXISTS fk_empresa')
            await pg.query('ALTER TABLE IF EXISTS empresa_funcionario DROP CONSTRAINT IF EXISTS fk_funcionario')
          })
          .then(async ()=>{
            expect(true).toBe(true)
            //await pg.end()
            done()
          })
          .catch(async () => {
            expect(false).toBe(true)
            //await pg.end()
            done()
          })
      })
      it('DELETAR TABELAS', async done => {
        
        pg.connect()
        .then(async ()=>{
          await pg.query('DROP TABLE IF EXISTS empresa_funcionario')
          await pg.query('DROP TABLE IF EXISTS empresa')
          await pg.query('DROP TABLE IF EXISTS funcionario')
        })
        .then(async ()=>{
          expect(true).toBe(true)
          //await pg.end()
          done()
        })
        .catch(async () => {
          expect(false).toBe(true)
          //await pg.end()
          done()
        })
    })
    it('CRIAR TABELAS', async done => {
      
      pg.connect()
      .then(async ()=>{
        await pg.query("CREATE TABLE empresa (id serial primary key,nome VARCHAR(255) NOT NULL,cnpj VARCHAR(20),endereco text)")
        await pg.query('CREATE TABLE funcionario (id serial primary key,nome VARCHAR(255) NOT NULL,cpf VARCHAR(15),email VARCHAR(255),endereco text)')
        await pg.query('CREATE TABLE empresa_funcionario(empre_id INT NOT NULL,funci_id INT NOT NULL,PRIMARY KEY(empre_id,funci_id))')
      })
      .then(async ()=>{
        expect(true).toBe(true)
        //await pg.end()
        done()
      })
      .catch(async () => {
        expect(false).toBe(true)
        //await pg.end()
        done()
      })
  })
  it('CRIAR CONSTRAINT', async done => {
    
    pg.connect()
    .then(async ()=>{
      await pg.query('ALTER TABLE empresa_funcionario ADD CONSTRAINT fk_empresa FOREIGN KEY(empre_id) REFERENCES empresa(id) ON DELETE CASCADE')
      await pg.query('ALTER TABLE empresa_funcionario ADD CONSTRAINT fk_funcionario FOREIGN KEY(funci_id) REFERENCES funcionario(id) ON DELETE CASCADE')
    })
    .then(async ()=>{
      expect(true).toBe(true)
      //await pg.end()
      done()
    })
    .catch(async () => {
      expect(false).toBe(true)
      //await pg.end()
      done()
    })
})
})

