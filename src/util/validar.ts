  /**
   * Converte string em array de objects para ordenação do prisma
   * 
   * EX: de validacao com express-validator
   * 
   * query( "ordem","ordem é opcional ou 'nome,asc;cnpj,desc'"
   * ).optional()
   * .custom((_ordem)=>{
   *   return ordenarQueryToArObj(_ordem,["nome","cnpj","endereco"])
   *     .filter((x:object)=>{return x===undefined})
   *     .length===0;
   * })
   * 
   *
   * EX: de utilizacao no codigo utilizando filtro para retirar undefined
   * 
   * let ordem = ordenarQueryToArObj(req.query.ordem,["nome","cnpj","endereco"])
   *      .filter((x:object)=>{return x!=undefined})
   * 
   * 
   * @param _ordem string query do express
   * @param _campos array de string com nome dos atributos da tabela
   * @returns array com objects ou undefined para invalidos
   */
  export const ordenarQueryToArObj = function ordenarQueryToArObj(_ordem:any,_campos:string[]){
    if(_ordem==undefined) return [];
    return _ordem.split(";")
    .map((x:string)=>{ return x.split(",");})
    .map((x:[string,string])=>{
      if(Array.isArray(x))
        if(x.length=2)
          if(_campos.indexOf(x[0])>=0&&["desc","asc",undefined].indexOf(x[1])>=0){
            return {[x[0]]:(x[1]||"asc")};
          }
    });
}


/**
 * function isValidEmail(_mail:string):boolean
 * @param _mail String
 * @returns boolean
 */
export const isValidEmail = function isValidEmail(_mail:string):boolean {
    
    // option us,br
    //const expEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    // option us,br,jp,ch,ru,рф,...
    const expEmail = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; 

    if (expEmail.test(_mail))
        return true;
    
    return false;
}

/**
 * function isValidCPF(_cpf:string):boolean
 * @param _cpf String
 * @returns boolean
 */
export const isValidCPF = function isValidCPF(_cpf:string):boolean {
    
    const cpf = _cpf.replace(/[\s.-]*/igm, '')

    if (cpf == "") 
        return false;

    if(cpf.length != 11)
        return false;

    if( cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" ||
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" ||
        cpf == "99999999999"   
        )        
        return false;
    
    let soma = 0
    let resto
    for (let i = 1; i <= 9; i++) 
        soma = soma + parseInt(cpf.substring(i-1, i)) * (11 - i)
    resto = (soma * 10) % 11
    if ((resto == 10) || (resto == 11))  resto = 0
    if (resto != parseInt(cpf.substring(9, 10)) ) 
        return false
    soma = 0
    for (let i = 1; i <= 10; i++) 
        soma = soma + parseInt(cpf.substring(i-1, i)) * (12 - i)
    resto = (soma * 10) % 11
    if ((resto == 10) || (resto == 11))  resto = 0
    if (resto != parseInt(cpf.substring(10, 11) ) ) 
        return false
    
    return true
}

/**
 * function isValidCNPJ(_cnpj:string):boolean
 * @param _cnpj String
 * @returns boolean
 */
export const isValidCNPJ = function isValidCNPJ(_cnpj:string):boolean{
 
    const cnpj:string = _cnpj.replace(/[\s.-//]*/igm,''); // replace(/[^\d]+/g,'');
 
    if(cnpj == "") 
        return false;
     
    if (cnpj.length != 14)
        return false;
 
    if (cnpj == "00000000000000" || 
        cnpj == "11111111111111" || 
        cnpj == "22222222222222" || 
        cnpj == "33333333333333" || 
        cnpj == "44444444444444" || 
        cnpj == "55555555555555" || 
        cnpj == "66666666666666" || 
        cnpj == "77777777777777" || 
        cnpj == "88888888888888" || 
        cnpj == "99999999999999")
        return false;
    
    let tamanho = cnpj.length - 2
    let numeros = cnpj.substring(0,tamanho);
    let digitos = cnpj.substring(tamanho);
    let soma = 0;
    let pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
        soma += Number.parseInt(numeros.charAt(tamanho - i)) * pos--;
        if (pos < 2) pos = 9;
    }
    let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != Number.parseInt(digitos.charAt(0)))
        return false;
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0,tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (let i = tamanho; i >= 1; i--) {
        soma += Number.parseInt(numeros.charAt(tamanho - i)) * pos--;
        if (pos < 2) pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != Number.parseInt(digitos.charAt(1)))
        return false;
    
    return true;
}