import express, { Request, Response } from "express";
import {  FindManyfuncionarioArgs, funcionarioCreateInput,  PrismaClient } from '@prisma/client'
import { body, param, query, validationResult } from "express-validator";
import {isValidCPF,isValidEmail,ordenarQueryToArObj} from "../util/validar";

/**
 * Router Definition
 */
export const funcionariosRouter = express.Router();


const prisma = new PrismaClient()

/**
 * Controller Definitions
 */

// GET funcionarios/
  const atributosOrdevaveis = ["nome","cpf","email","endereco"];
  const atributosProcuraveis = atributosOrdevaveis;
  funcionariosRouter.get("/",
  [
    query("pagina","pagina é opcional ou deve ser um numero inteiro,"+
          " 1 <= pagina").optional().trim().isInt({gt:0}),
    query("limite","limite é opcional ou deve ser um numero inteiro,"+
          " 0 <= limite <= 100").optional().trim().isInt({gt:-1,lt:101}),
    query("procurar","procurar é opcional ou deve conter"+
          " no minimo 2 caracteres").optional().trim().isLength({min:2}),
    query("ordem","ordem é opcional ou deve conter um ou mais"+
          " ([nome|cnpj|endereco][,asc|,desc];) EX:'nome,asc;cnpj,desc'").optional().trim()
          .custom((_ordem)=>{
            return ordenarQueryToArObj(_ordem,atributosOrdevaveis)
              .filter((x:object)=>{return x===undefined})
              .length===0;
          }
    ),
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
      let op : FindManyfuncionarioArgs = {
        include: { empresa_funcionario : {include:{empresa:true}} }
      };

      // paginar
      let _pagina = 1;
      let _limite = 0;
      if (req.query && req.query.pagina) { _pagina = Number.parseInt(String(req.query.pagina)) || 1;}
      if (req.query && req.query.limite) { _limite = Number.parseInt(String(req.query.limite)) || 0;}
      
      if(_limite>0){
        op.skip = ((_pagina-1)*_limite);
        op.take = _limite;
      }

      // procurar
      if(req.query && req.query.procurar && String(req.query.procurar).length > 1){
        let procurar = atributosProcuraveis.map((x:any)=>{
          return {[x]:{contains:req.query.procurar,mode: "insensitive"}};
        })
        op.where = {OR:procurar}
      }
      
      // ordenar
      let ordem = ordenarQueryToArObj(req.query.ordem,atributosOrdevaveis)
                      .filter((x:object)=>{return x!=undefined})
      if(ordem.length>0) op.orderBy = ordem

      //banco de dados
      await prisma.funcionario.findMany(op).then((values)=>{
          res.status(200).json(values);
      }).catch((e)=>{
        res.status(500).json({ erro:e.message});
      })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });

  // GET funcionarios/:id
  
  funcionariosRouter.get("/:id",
  [
    param("id","id é obrigatorio, deve ser numerico inteiro, 0 <= id").isInt({gt:0})
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const id: number = parseInt(req.params.id, 10);

        //banco de dados
        await prisma.funcionario.findOne({
            where:{id:id},
            include: { empresa_funcionario : {include:{empresa:true}} }
        }).then((values)=>{
            res.status(200).json(values);
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
  
  
  // POST funcionarios/

  funcionariosRouter.post("/",
    [
      body("nome","Nome é obrigatório ou deve ter no minimo 2 caracteres").trim().isLength({min:2}),
      body("cpf","CPF é opcional ou deve ser valido").optional().trim().custom(isValidCPF),
      body("email","Email é opcional ou deve ser uma string valida").optional().trim().custom(isValidEmail),
      body("endereco","Endereço é opcional ou deve conter no minimo 2 caracteres").optional().isLength({min:2}),
      body("empresas","Empresas é opcional ou deve conter um array de strings de numeros inteiros ids das empresas").optional().isArray(),
      ],
    async (req: Request, res: Response) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).json({ form_erros: errors.array() });
      }
      try {
        const {nome,cpf,email, endereco, empresas} = req.body;
        
        const data:funcionarioCreateInput = {
          nome:nome
        };
        
        if(cpf!=undefined){data.cpf = cpf.replace(/[\s.-]*/igm, '');}
        if(email!=undefined){data.email = email;}
        if(endereco!=undefined){data.endereco = endereco;}

        //Adiciona relação de funcionario com empresa
        const create_empresa_funcionario = new Array();
        if(Array.isArray(empresas)&&empresas.length>0){
          Array.from(new Set(empresas)).forEach((emp)=>{
            const empre_id = Number.parseInt(emp);
            if(!isNaN(empre_id))
              create_empresa_funcionario.push(
                {empresa:{connect:{id:empre_id}}}
              );
          })
          if(create_empresa_funcionario.length>0)
            data.empresa_funcionario = {create:create_empresa_funcionario}
        }

        //banco de dados
        await prisma.funcionario.create({
            data:data
        }).then((funcionario)=>{
          res.status(200).json(funcionario);
        }).catch((e)=>{
            res.status(500).json({ erro:e.message});
        })
      } catch (e) {
        res.status(400).json({ erro:e.message});
      }
    }
  );
  
  // PUT funcionarios/
  
  funcionariosRouter.put("/:id",
  [
    param("id","id é obrigatorio, deve ser numerico inteiro, 0 <= id").isInt({gt:0}),
    body("nome","Nome é opcional ou deve ter no minimo 2 caracteres").optional().trim().isLength({min:2}),
    body("cpf","CPF é opcional ou deve ser valido").optional().trim().custom(isValidCPF),
    body("email","Email é opcional ou deve ser uma string valida").optional().trim().custom(isValidEmail),
    body("endereco","Endereço é opcional ou deve conter no minimo 2 caracteres").optional().isLength({min:2}),
    body("empresas","Empresas é opcional ou deve conter um array de strings de numeros inteiros ids das empresas").optional().isArray()
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const id: number = parseInt(req.params.id, 10);
        const {nome,cpf,email, endereco, empresas} = req.body;

        const data : {[k: string]: any} = {};
        if(nome!=undefined){data.nome = nome;}
        if(cpf!=undefined){data.cpf = cpf.replace(/[\s.-]*/igm, '');}
        if(email!=undefined){data.email = email;}
        if(endereco!=undefined){data.endereco = endereco;}
        
        //Adiciona relação de funcionario com empresa
        const create_empresa_funcionario = new Array();
        if(Array.isArray(empresas)&&empresas.length>0){
          Array.from(new Set(empresas)).forEach((emp)=>{
            const empre_id = Number.parseInt(emp);
            if(!isNaN(empre_id))
              create_empresa_funcionario.push(
                {empresa:{connect:{id:empre_id}}}
              );
          })
          if(create_empresa_funcionario.length>0)
            data.empresa_funcionario = {create:create_empresa_funcionario}
        }

        //banco de dados
        await prisma.empresa_funcionario.deleteMany({
          where:{funci_id:id}
        }).then(async ()=>{
          //banco de dados
          await prisma.funcionario.update({
              where:{id:id},
              data:data
          }).then((values)=>{
              res.status(200).json(values);
          }).catch((e)=>{
              res.status(500).json({ erro:e.message});
          })
        }).catch((e)=>{
            res.status(500).json({ erro:e.message});
        })
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
  
  // DELETE funcionarios/:id
  
  funcionariosRouter.delete("/:id",
  [
    param("id","id é obrigatorio, deve ser numerico inteiro, 0 <= id").isInt({gt:0})
  ],
  async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ form_erros: errors.array() });
    }
    try {
        const id: number = parseInt(req.params.id, 10);

        //banco de dados
        await prisma.empresa_funcionario.deleteMany({
            where:{funci_id:id}
        }).then(async (emp_fun)=>{
            //banco de dados
            await prisma.funcionario.delete({
              where:{id:id}
            }).then(()=>{
              res.status(200).json({});
              return;
            }).catch((e)=>{
              res.status(500).json({ erro:e.message});
            });
        }).catch((e)=>{
          res.status(500).json({ erro:e.message});
        });
    } catch (e) {
      res.status(400).json({ erro:e.message});
    }
  });
