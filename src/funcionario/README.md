# Como utilizar a API Funcionario

## URLs Funcionario

* GET   http://127.0.0.1:8000/funcionario
* GET   http://127.0.0.1:8000/funcionario/(id)
* POST  http://127.0.0.1:8000/funcionario
* PUT   http://127.0.0.1:8000/funcionario/(id)
* DEL   http://127.0.0.1:8000/funcionario/(id)

# Como utilizar a API Funcionario

### GET http://127.0.0.1:8000/funcionario

Parametros diretamente na url ou utilizando Query do Insomnia/postman
* pagina - pagina é opcional ou deve ser um numero inteiro, 1 <= pagina
* limite - limite é opcional ou deve ser um numero inteiro, 0 <= limite <= 100
* procurar - procurar é opcional ou deve conter no minimo 2 caracteres
* ordem - ordem é opcional ou deve conter um ou mais ([nome|endereco][,asc|,desc];) EX:'nome,asc;endereco,desc'


Exemplo
```code
GET http://127.0.0.1:8000/funcionario?pagina=1&limite=2&ordem=nome,asc;endereco,desc&procurar=Rua
```
Codigo 200 - Retorno com sucesso
```code
[]

ou

[
    {
        "id": 4,
        "nome": "Funcionario 03",
        "cpf": "22265369s810",
        "email": "helio@s1te.com.br",
        "endereco": "Rua Prof. Herminio Jacques 229",
        "empresa_funcionario": [
        {
            "empre_id": 1,
            "funci_id": 4,
            "empresa": {
            "id": 1,
            "nome": "Empresa 01",
            "cnpj": "12345678921",
            "endereco": "Rua Prof. Herminio Jacques 300"
            }
        }
        ]
    }
]
```
Codigo 400 - Resposta de erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "",
      "msg": "pagina é opcional ou deve ser um numero inteiro, 1 <= pagina",
      "param": "pagina",
      "location": "query"
    },
    {
      "value": "",
      "msg": "limite é opcional ou deve ser um numero inteiro, 0 <= limite <= 100",
      "param": "limite",
      "location": "query"
    },
    {
      "value": "",
      "msg": "procurar é opcional ou deve conter no minimo 2 caracteres",
      "param": "procurar",
      "location": "query"
    },
    {
      "value": "",
      "msg": "ordem é opcional ou deve conter um ou mais ([nome|cnpj|endereco][,asc|,desc];) EX:'nome,asc;cnpj,desc'",
      "param": "ordem",
      "location": "query"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

### GET http://127.0.0.1:8000/funcionario/(id)
* id - id é obrigatorio, deve ser numerico inteiro, 0 <= id existente

Exemplo
```code
GET http://127.0.0.1:8000/funcionario/1
```

Codigo 200 - Retorno com sucesso
```code
null

OU

{
  "id": 1,
  "nome": "Funcionario 01",
  "cpf": "22265369810",
  "email": "helio@s1te.com.br",
  "endereco": "Rua Prof. Herminio Jacques 229",
  "empresa_funcionario": [
    {
      "empre_id": 1,
      "funci_id": 3,
      "empresa": {
        "id": 1,
        "nome": "Empresa 01",
        "cnpj": "12345678921",
        "endereco": "Rua Prof. Herminio Jacques 300"
      }
    }
  ]
}
```
Codigo 400 - Resposta de erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "-3",
      "msg": "id é obrigatorio, deve ser numerico inteiro, 0 <= id",
      "param": "id",
      "location": "params"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

### DEL http://127.0.0.1:8000/funcionario/(id)
* id - id é obrigatorio, deve ser numerico inteiro, 0 <= id existente

Exemplo
```code
DEL http://127.0.0.1:8000/funcionario/3
```
Codigo 200 - Retorno com sucesso
```code
{}
```
Codigo 400 - Resposta de erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "-3",
      "msg": "id é obrigatorio, deve ser numerico inteiro, 0 <= id",
      "param": "id",
      "location": "params"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Error for binding '0': RecordNotFound(\"Record to delete does not exist.\")"
}
```

### POST http://127.0.0.1:8000/funcionario
Parametros POST json
* nome - Nome é obrigatório ou deve ter no minimo 2 caracteres
* cpf - CPF é opcional ou deve ser valido
* email - Email é opcional ou deve ser uma string valida
* endereco - Endereço é opcional ou deve conter no minimo 2 caracteres
* empresas - Empresas é opcional ou deve conter um array de strings de numeros inteiros ids das empresas

Exemplo POST com json:
```code
POST http://127.0.0.1:8000/funcionario

{
	"nome":"nome",
	"cpf":"22265369810",
	"email":"helio@s1te.com.br",
	"endereco":"Rua da Avenida movimentada, 22",
	"empresas":["1"]
}
```
Codigo 200 - Retorno com sucesso
```code
{
  "id": 7,
  "nome": "nome",
  "cpf": "22265369810",
  "email": "helio@s1te.com.br",
  "endereco": "Rua da Avenida movimentada, 22"
}
```
Codigo 400 - Resposta de erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "",
      "msg": "Nome é obrigatório",
      "param": "nome",
      "location": "body"
    },
    {
      "value": "",
      "msg": "CPF invalido",
      "param": "cpf",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Email invalido",
      "param": "email",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Empresas é opcional ou deve conter um array de strings de numeros inteiros, IDs",
      "param": "empresas",
      "location": "body"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

### PUT http://127.0.0.1:8000/funcionario/(id)
Parametros PUT json
* id - id é obrigatorio, deve ser numerico inteiro, 0 <= id existente
* nome - Nome é obrigatório ou deve ter no minimo 2 caracteres
* cpf - CPF é opcional ou deve ser valido
* email - Email é opcional ou deve ser uma string valida
* endereco - Endereço é opcional ou deve conter no minimo 2 caracteres
* empresas - Empresas é opcional ou deve conter um array de strings de numeros inteiros ids das empresas

Exemplo PUT com json:
```code
PUT http://127.0.0.1:8000/funcionario/7

{
	"nome":"Joao da Silva",
	"cpf":"12049776071",
	"email":"joao@silva.com.br",
	"endereco":"Avenida Travada, 999",
	"empresas":["1","2"]
}
```
Codigo 200 - Retorno com sucesso
```code
{
  "id": 7,
  "nome": "Joao da Silva",
  "cpf": "12049776071",
  "email": "joao@silva.com.br",
  "endereco": "Avenida Travada, 999"
}
```
Codigo 400 - Erros de parametros enviados para API
```code
{
  "form_erros": [
    {
      "value": "-7",
      "msg": "id é obrigatorio, deve ser numerico inteiro, 0 <= id",
      "param": "id",
      "location": "params"
    },
    {
      "value": "",
      "msg": "Nome é opcional ou deve ter no minimo 2 caracteres",
      "param": "nome",
      "location": "body"
    },
    {
      "value": "",
      "msg": "CPF é opcional ou deve ser valido",
      "param": "cpf",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Email é opcional ou deve ser uma string valida",
      "param": "email",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Endereço é opcional ou deve conter no minimo 2 caracteres",
      "param": "endereco",
      "location": "body"
    },
    {
      "value": "",
      "msg": "Empresas é opcional ou deve conter um array de strings de numeros inteiros ids das empresas",
      "param": "empresas",
      "location": "body"
    }
  ]
}
```
Codigo 500 - Resposta de erro relacionados ao banco de dados
```code
{
  "erro": "Please make sure your database server is running at `localhost`:`5432`"
}
```

