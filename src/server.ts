import {server} from "./app"


const server_port = 8000;

server.listen(server_port, () => {
  console.log("Example app listening on port 5678!");
});